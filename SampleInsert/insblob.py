from __future__ import print_function
from datetime import date, datetime, timedelta
import mysql.connector
import base64

cnx = mysql.connector.connect(host='ec2-184-73-108-143.compute-1.amazonaws.com',
      database='db1',user='root',password='dUmp1ngdata')
cursor = cnx.cursor()


tab1_stmt = ("INSERT INTO db1.parking_images "
     "(parkinglot_id, parking_image) "
     "VALUES (%s, %s)" )

with open("C:\mysql-logo.png", "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())



tab1_data = (40, encoded_string)

cursor.execute(tab1_stmt , tab1_data)
#cursor.execute(add_tab1)


cnx.commit()

cursor.close()
cnx.close()
