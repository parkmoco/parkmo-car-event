from __future__ import print_function
from datetime import date, datetime, timedelta
import mysql.connector



cnx = mysql.connector.connect(user='pppuser', database='db1',password='pppuser!', 
     host='inst1.c8zfqmiqtr7f.us-east-1.rds.amazonaws.com')
cursor = cnx.cursor()
tomorrow = datetime.now().date() + timedelta(days=1)

add_employee = ("INSERT INTO db1.employees "
 "(first_name, last_name, hire_date, gender, birth_date) "
 "VALUES (%s, %s, %s, %s, %s)")

data_employee = ('Bob', 'Vanderkelen', tomorrow, 'M', date(1977, 6, 14))
# Insert new employee
cursor.execute(add_employee, data_employee)

# Make sure data is committed to the database
cnx.commit()
cursor.close()
cnx.close()
