""" DBInterface.py """
import json
import mysql.connector

class DBInterface:
    """ dbInterface class """
    def __init__(self):
        self.cnx = ""
        self.cursor = ""
        with open("config.json", "r") as infile:
            self.config = json.load(infile)
        self.connection_server = self.config["connection_server"]
        #self.connection = self.config["connection"]
        self.connection = self.config[self.connection_server]
        print("connection: ", self.connection)
        self.host = self.connection["host"]
        self.database = self.connection["database"]
        self.user = self.connection["user"]
        self.password = self.connection["password"]
        self.parkinglot_id = self.config["parkinglot_id"]
        self.inout = self.config["inout"]
        self.gpscoords = self.config["gpscoords"]
        self.lat = self.gpscoords["lat"]
        self.lng = self.gpscoords["lng"]
        if str(self.inout).lower() == "in":
            self.stmt_ce = (
                "INSERT INTO " + self.database + ".dailyparking"
                "(parkingLotID, lat, lng, inlot, outlot, photo) "
                "VALUES (%s, %s, %s, NOW(), NULL, %s)")
        else:
            self.stmt_ce = (
                "INSERT INTO " + self.database + ".dailyparking"
                "(parkingLotID, lat, lng, inlot, outlot, photo) "
                "VALUES (%s, %s, %s, NULL, NOW(), %s)")
        self.stmt_hb = (
            "INSERT INTO " + self.database + ".heartbeat"
            "(macID, time, cpuLoad, ramUsage, ramTotal, linkQual, "
            "signLvl, noiseLvl, battPcnt, battC, "
            "battV, battTc, cpuTc)"
            "VALUES (%s, NOW(), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")


    def connect(self):
        """ Connect to AWS mysql database """
        self.cnx = mysql.connector.connect(
            host=self.host,
            database=self.database,
            user=self.user,
            password=self.password)
        self.cursor = self.cnx.cursor()

    def disconnect(self):
        """ disconnect from AWS connection """
        self.cursor.close()
        self.cnx.close()

    def insert_car_event(self, image):
        """ Insert car event into AWS """

        image_file = open(image, "rb")
        img = image_file.read()
        data = (self.parkinglot_id, self.lat, self.lng, img)

        # worked for png, but not jpg
        #with open(image, "rb") as image_file:
        #    encoded_string = base64.b64encode(image_file.read())
        #data = (self.parkinglot_id, self.lat, self.lng, encoded_string)

        self.cursor.execute(self.stmt_ce, data)
        print("Insert: ", self.stmt_ce)
        self.cnx.commit()

    # def select_car_event(self, id):
    #     """ select car event into AWS """
    #     sel_stmt = (
    #         "SELECT parkingLotID, lat, lng, inlot, outlot, photo "
    #         "FROM " + self.database + ".dailyparking "
    #         "where parkingLotID = %s")
    #     self.cursor.execute(sel_stmt)
    #     data=self.cursor.fetchall()

    #     print ("Data0: " , data[0][0])
    #     print ("Type0: " , type(data[0][0]))
    #     print ("Data1: " , data[0][1])
    #     print ("Type1: " , type(data[0][1]))

    #     data1=base64.b64decode(data[0][1])
    #     #data1=base64.b64decode(data[0])
    #     print ("Data2: " , data1)
    #     print ("Type2: " , type(data1))

    #     webbrowser.open(data1)
 
    #     db.close()

    def insert_heartbeat(self, mac_id, cpu_load, ram_usage,
                         ram_total, link_qual, sign_lvl, noise_lvl, batt_pcnt,
                         batt_c, batt_v, batt_tc, cpu_tc):
        """ Insert heartbeat into AWS """
        data = (mac_id, cpu_load, ram_usage, ram_total, link_qual,
                sign_lvl, noise_lvl, batt_pcnt, batt_c, batt_v,
                batt_tc, cpu_tc)
        self.cursor.execute(self.stmt_hb, data)
        print("Insert: ", self.stmt_hb)
        self.cnx.commit()
